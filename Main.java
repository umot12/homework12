package homework8;

import homework8.DAO.CollectionFamilyDao;
import homework8.DAO.FamilyController;
import homework8.DAO.FamilyDao;
import homework8.DAO.FamilyService;
import homework8.Family.Dog;
import homework8.Family.Family;
import homework8.Family.Human;
import homework8.Family.RoboCat;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main  {
    public static void main(String[] args) throws ParseException {
        List<Family> familyList = new ArrayList<>(List.of());
        FamilyDao familyDao = new CollectionFamilyDao(familyList);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);
        familyController.start();

    }
}
