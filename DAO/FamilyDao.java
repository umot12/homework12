package homework8.DAO;

import homework8.Family.Family;

import java.util.List;

 public interface FamilyDao  {

       public void getBatha();

      public List<Family> getAllFamilies();
    public Family getFamilyByIndex(int indexOfFamily);

    public boolean deleteFamily(int indexOfFamily);

    public boolean deleteFamily(Family family);

    public void saveFamily(Family family);




}
